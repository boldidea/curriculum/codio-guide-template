const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: {
    main: path.resolve(__dirname, './src/index.js'),
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js',
  },
  resolve: {
    alias: {
      'codedoc': path.resolve(__dirname, 'node_modules/codedoc'),
      'intro.js': path.resolve(__dirname, 'node_modules/intro.js')
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    open: true,
    compress: true,
    //hot: true,
    port: 8080,
    before: function(app, server, compiler) {
      app.get('/', function(rqe, res) {
        res.set('Content-Type', 'text/html');
        const head = fs.readFileSync(path.resolve(__dirname, './src/header.html'));
        const test = fs.readFileSync(path.resolve(__dirname, './src/test.html'));
        const footer = fs.readFileSync(path.resolve(__dirname, './src/footer.html'));
        let html = head + test + footer;

        // manually inject bundle
        bundleTag = '<script defer src="main.bundle.js"></script>'
        html = html.replace('</head>', bundleTag + '</head>');

        res.send(Buffer(html));
      });
    }
  },
  plugins: [
    /*
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/header.html'),
      filename: 'header.html'
    }),
    */
    new CopyWebpackPlugin({
      patterns: [
        {from: './src/header.html', to: 'header.html'},
        {from: './src/footer.html', to: 'footer.html'},
        {from: './src/css/style.css', to: 'css/style.css'},
        {from: './src/css/screen.css', to: 'css/screen.css'},
      ]
    }),
    new CleanWebpackPlugin(),
    //new webpack.HotModuleReplacementPlugin()
  ]
};
