import CodeDoc from 'codedoc';
import introJs from 'intro.js';

import 'codedoc/src/css/colors.css';
import 'codedoc/src/css/code-highlight.css';
import 'codedoc/src/css/style.css';
import 'intro.js/dist/introjs.css';
import './css/style.css';
import './css/screen.css';

window.introJs = introJs;

const opts = {
  containerNames: ['quiz', 'quiz-question', 'quiz-final'],
};

const doc = new CodeDoc(opts);
window.doc = doc;
window.codioUtils = {};

function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

// store for postMessage callbacks
const postMessageCallbacks = {};
function registerCallback(callback) {
  const id = uuid();
  postMessageCallbacks[id] = callback;
  return id;
}

window.codioUtils = {
  closePreviewTab: (url) => {
    window.parent.postMessage({
      action: 'closePreviewTab',
      url: url
    }, '*');
  },

  addMenuItem: (path, position, label, callback) => {
    let callbackId = null;
    if (callback) {
      callbackId = registerCallback(callback);
    }
    window.parent.postMessage({
      action: 'addMenuItem',
      path, position, label, callbackId
    }, '*');
  },

  addGuideMenuItem: (label, url) => {
    window.parent.postMessage({
      action: 'addGuideMenuItem',
      label, url
    }, '*');
  },

  getGuidePanelIndex: () => {
    return new Promise((resolve, reject) => {
      const callbackId = registerCallback(index => resolve(index));
      window.parent.postMessage({
        action: 'getGuidePanelIndex',
        callbackId
      }, '*');
    })
  },

  closeAllTabs: (panel) => {
    const msg = {action: 'closeAllTabs'}
    if (typeof panel !== 'undefined') msg.panel = panel;
    window.parent.postMessage(msg, '*');
  },

  selectGuideTab: () => {
    window.parent.postMessage({action: 'selectGuideTab'}, '*');
  },
}

/**
 * Executes on DOMContentLoaded
 */
function domReady() {
  const container = document.querySelector('#chapterContent');
  const markdownEl = document.querySelector('script[type="text/markdown"]');
  let elClasses = [];
  if (markdownEl.dataset.class) {
    const elClasses = markdownEl.dataset.class.split(' ');
  }

  document.querySelector('.currentYear').textContent = (new Date()).getFullYear();

  if (!markdownEl) {
    throw new Error('Could not find markdown element. Page must include a <script type="text/markdown"> element.');
  }

  doc.addEventListener('loaded', () => {
    document.body.classList.add('loaded');

    // set chapter div class name
    for (let className of elClasses) {
      document.querySelector('.chapter').classList.add(className);
    }

    for (const link of document.querySelectorAll('.codio-link')) {
      link.addEventListener('click', event => {
        if (link.dataset.section) {
          codio.goToSectionTitled(link.dataset.section);
        }
        event.preventDefault();
      });
    }

    // Set up preview buttons
    for (const btn of document.querySelectorAll('a.preview')) {
      btn.addEventListener('click', event => {
        event.preventDefault();
        const url = btn.getAttribute('href');
        const panel = btn.dataset.panel;
        codio.open('preview', url, panel);
      });
    }

    // Set up reset buttons
    for (const btn of document.querySelectorAll('a.reset-files')) {
      btn.addEventListener('click', event => {
        event.preventDefault();
        codio.resetCurrentFiles();
      });
    }

    // Set external links to open in new tab
    for (const a of document.querySelectorAll('a')) {
      let url;
      try {
        url = new URL(a.getAttribute('href'));
      } catch {
        // ignore invalid urls
        continue;
      }
      if (url.hostname && !url.hostname.match(/codio.(com|io)$/)) {
        a.setAttribute('target', '_blank');
      }
    }
  });


  let markdown = markdownEl.textContent;

  // replace escaped </script> tags
  window.markdown = markdown;
  markdown = markdown.replace('<\\/script>', '</script>');

  doc.init(container, markdown);

  // Setup nav buttons
  const backBtn = document.querySelector('#navPrev'); 
  const nextBtn = document.querySelector('#navNext'); 
  const navContainer = nextBtn.closest('nav');

  backBtn.addEventListener('click', event => {
    navContainer.style.display = 'none';
    event.preventDefault();
    codio.goToPreviousSection();
  });

  nextBtn.addEventListener('click', event => {
    navContainer.style.display = 'none';
    event.preventDefault();
    codio.goToNextSection();
  });

  // Get guide nav state
  window.addEventListener('message', event => {
    if (!event.origin.match(/codio.(io|com)$/)) return;
    let data;
    if (typeof event.data === 'string') {
      data = JSON.parse(event.data);
    } else {
      data = event.data;
    }
    if (data.action === 'guideNavState') {
      if (data.btn === 'prev') {
        backBtn.style.display = data.disabled ? 'none' : '';
        document.body.classList.add('first-page');
        document.body.classList.remove('last-page');
      } else if (data.btn === 'next') {
        nextBtn.style.display = data.disabled ? 'none' : '';
        document.body.classList.add('last-page');
        document.body.classList.remove('first-page');
      }
    }

    if (data.action === 'callback') {
      const callback = postMessageCallbacks[data.callbackId];
      if (callback) {
        const args = data.args || [];
        callback(...args);
      } else {
        // Not our callback, probably intended for codio-client.js
        return;
        //console.error('Invalid callback id', data.callbackId);
      }
    }
  });
  window.parent.postMessage({action: 'watchGuideNavState'}, '*');

}

// KLUDGE: See https://github.com/guybedford/es-module-shims/issues/99
if (window.__domReady) {
  domReady();
} else {
  document.addEventListener('DOMContentLoaded', domReady);
}

console.log('GUIDE TEMPLATE LOADED');
